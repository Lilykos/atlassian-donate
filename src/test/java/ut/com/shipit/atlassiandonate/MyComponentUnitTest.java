package ut.com.shipit.atlassiandonate;

import org.junit.Test;
import com.shipit.atlassiandonate.MyPluginComponent;
import com.shipit.atlassiandonate.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}